sleep 90s

# Restore de la base de datos andisacos en la imagen
# https://docs.microsoft.com/en-us/sql/linux/tutorial-restore-backup-in-sql-server-container?view=sql-server-ver15
/opt/mssql-tools/bin/sqlcmd \
-S localhost -U SA -P '123456Ps' \
-Q 'RESTORE DATABASE B00010006 FROM DISK = "/var/opt/mssql/backup/andisacos.bak" WITH MOVE "B00010006" TO "/var/opt/mssql/data/B00010006.mdf", MOVE "B00010006_log" TO "/var/opt/mssql/data/B00010006_log.ldf"'
